package linkedlist;

// Class to represent a LinkedList<T>
//   (Uses Node<T>)
public class LinkedList<T> {

	// Private head of list (starts out null)
	private Node<T> head = null;
	// Private count of items
	private int length = 0;
	
	// Constructor to create Linked List
	public LinkedList() {
		// Put constructor code here!
	}
	
	// Method to return boolean (true if list is empty)
	public boolean isEmpty() {
		return (head == null);
	}
	
	// Get the number of items in the list
	public int size() {
		return(length);
	}
	
	// Clear the list of all items
	public void clear() {
		head = null;
		length = 0;
	}
	
	// Method to get an item at a position in the list
	public T get1(int pos) {
		if (length < pos || pos <= 0)
			return null;
		else {
			Node<T> p = head;
			for (int i = 1; i < pos; i++)
				p = p.getNext();
			return p.getData();
		}
	}
	
	// Method to get a node containing an item
	public Node<T> get2(T data) {
		Node<T> ret = null;
		Node<T> p = head;
		while (p != null && !p.getData().equals(data))
			p = p.getNext();
		if (p != null)
			ret = p;
		return ret;
	}
	
	/*
	 * Methods of ADT List to implement
	 * 
	 * +Create
	 * +isEmpty
	 * +Size
	 * +Get
	 * +Clear
	 * Add
	 * Remove
	 * 
	 * Other ideas:
	 * 
	 * Contains
	 * toString
	 */
	
}
