package linkedlist;

import org.junit.Test;

// Class to test Node<T>
public class TestNodeList extends junit.framework.TestCase {
	
	@Test
	public void testNodeNotNull() {
		assertNotNull("Should not be null",new Node<Integer>(3));
	}

	@Test
	public void testListNotNull() {
		assertNotNull("Should not be null",new LinkedList<Integer>());
	}

	@Test
	public void testListEmpty() {
		LinkedList<String> lst = new LinkedList<String>();
		assertTrue(lst.isEmpty());
	}
	
	@Test
	public void testSizeZero() {
		LinkedList<String> lst = new LinkedList<String>();
		assertEquals(lst.size(),0);
	}

}
