package oop.polymorphism;

import oop.inheritance.*;

public class Test {
  public static void main(String[] args) {
	A v1 = new A();
	A v2 = new B();
	A v3 = new C();
	B v4 = new B();
	B v5 = new C();
	C v6 = new C();
	SayHelloable s1 = new B();
	SayHelloable s2 = new C();
	//C v7 = new A();
	
	// Test Interface 1
	v4.sayHello();
	v5.sayHello();
	v6.sayHello();
	s1.sayHello();
	s2.sayHello();
	
	// Test Interface 2
	v4.sayHello2();
	v5.sayHello2();
	v6.sayHello2();
	// Bad!
	//s1.sayHello2();
	// Bad!
	//s2.sayHello2();	
	
	System.out.println("v1: " + v1);
	System.out.println("v2: " + v2);
	System.out.println("v3: " + v3);
	System.out.println("v4: " + v4);
	System.out.println("v5: " + v5);
	System.out.println("v6: " + v6);

  }
}
