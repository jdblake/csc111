package oop.encapsulation;

public class B extends A {

  public String className = "B";
  public String bData = "B Data";
  private String justB = "Just B";
  protected String bSub = "B and Sub";
  String bPackage = "B and Package";

  public String toString() {
	return("Class name: " + className);
  }
}
