package oop.encapsulation;

public class A {

	public String className = "A";
	public String aData = "A Data";
	private String justA = "Just A";
	protected String aSub = "A and Sub";
	String aPackage = "A and Package";

	public String toString() {
		return ("Class name: " + className);
	}

	public void test() {
		/*
		** Variables in B
        public String className = "B";
        public String bData = "B Data";
        private String justB = "Just B";
        protected String bSub = "B and Sub";
        String bPackage = "B and Package";
		 */
		
		// Make a variable of type B
		B testB = new B();
		
		System.out.println(testB.className);
		System.out.println(testB.bData);
		System.out.println(testB.bSub);
		System.out.println(testB.bPackage);
		//System.out.println(testB.justB);
		
	}
}
