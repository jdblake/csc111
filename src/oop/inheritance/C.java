package oop.inheritance;

public class C extends B {

  public String className = "C";

  public String toString() {
	return("Class name: " + className);
  }
}
