package oop.inheritance;

public class B extends A implements SayHelloable {

  public String className = "B";
  
  public void sayHello() {
	  System.out.println("Hello!");
  }

  public void sayHello2() {
	  System.out.println("Hello 2!");
  }

  public String toString() {
	return("Class name: " + className);
  }
}
