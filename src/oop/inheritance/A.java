package oop.inheritance;

public class A {

  public String className = "A";

  public String toString() {
	return("Class name: " + className);
  }
}
