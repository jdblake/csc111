/*
 * Class to recursively build a simple maze with no doors
 */

package maze;

import java.util.ArrayList;

public class MazeBuilderRecursive extends MazeBuilderGeneric {
	
	// Basic constructor
	public MazeBuilderRecursive(int h, int w) {
		height = h;
		width = w;
		init();
	}
	
	// Method to set the starting position and build the maze
	@Override
	public void buildMaze() {
		// Set start of maze
		start = new int[2];
		start[0] = (int) (Math.random() * height);
		start[1] = (int) (Math.random() * width);
		buildMazeInternal(start);
	}
	
	// Recursive method to build maze from position
	private void buildMazeInternal(int[] pos) {
		
		// Visit current position
		visited[pos[0]][pos[1]] = true;

		// Randomize directions
		ArrayList<Integer> dirs = getPermutation();

		// Recursively try all directions
		while (!dirs.isEmpty()) {
			int val = dirs.remove(0);
			int[] newPos = { pos[0] + adds[val][0], pos[1] + adds[val][1] };
			// Try direction only if legal
			if (isLegal(newPos)) {
				walls[pos[0]][pos[1]] &= fromMask[val];
				walls[newPos[0]][newPos[1]] &= toMask[val];
				buildMazeInternal(newPos);
			}
		}

	}

}