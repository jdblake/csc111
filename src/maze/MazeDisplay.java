package maze;

import java.io.File;

import javax.imageio.ImageIO;

// Maze drawing adapted from:
//	http://docs.oracle.com/javafx/2/canvas/jfxpub-canvas.htm
//
// Image creation adapted from:
//  https://community.oracle.com/thread/2450090?tstart=0

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MazeDisplay extends Application {

	// Maze data
	MazeBuilderGeneric m = null;
	int h = 0;
	int w = 0;
	int boxSize = 40;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		// Build the maze
		h = 10;
		w = 10;
		//m = new MazeBuilderRecursive(h, w);
		m = new MazeBuilderStack(h, w);
		m.buildMaze();
		System.out.println(m);

		// Set the stage
		primaryStage.setTitle("Maze");
		Group root = new Group();
		Canvas canvas = new Canvas((h + 1) * boxSize, (w + 1) * boxSize);
        WritableImage wim = new WritableImage((h + 1) * boxSize, (w + 1) * boxSize);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		drawLines(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
		
        canvas.snapshot(null, wim);
        File file = new File("src/maze/MazeImage.png");

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
        } catch (Exception s) {
        }

	}

	private void drawLines(GraphicsContext gc) {

		gc.setStroke(Color.BLACK);
		gc.setLineWidth(2);

		// Get the maze
		byte[][] maze = m.getWalls();

		// Process the cells of the maze
		for (int r = 0; r < h; r++)
			for (int c = 0; c < w; c++) {
				// Determine the offset for a box
				int offsetY = (r + 1) * boxSize;
				int offsetX = (c + 1) * boxSize;
				byte cur = maze[r][c];
				// Draw each line in the box
				if ((1 & cur) != 0) {
					gc.strokeLine(offsetX - boxSize / 2,
							      offsetY - boxSize / 2,
							      offsetX + boxSize / 2,
							      offsetY - boxSize / 2);
				}
				if ((2 & cur) != 0) {
					gc.strokeLine(offsetX - boxSize / 2,
							      offsetY + boxSize / 2,
							      offsetX + boxSize / 2,
							      offsetY + boxSize / 2);
				}
				if ((4 & cur) != 0) {
					gc.strokeLine(offsetX + boxSize / 2,
							      offsetY - boxSize / 2,
							      offsetX + boxSize / 2,
							      offsetY + boxSize / 2);
				}
				if ((8 & cur) != 0) {
					gc.strokeLine(offsetX - boxSize / 2,
							      offsetY - boxSize / 2,
							      offsetX - boxSize / 2,
							      offsetY + boxSize / 2);
				}

			}
	}
}
