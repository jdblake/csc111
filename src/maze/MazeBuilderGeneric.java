package maze;

import java.util.ArrayList;

public abstract class MazeBuilderGeneric {

	// Maze height and width
	int height = 0;
	int width = 0;
	
	// Array of values for maze walls
	byte[][] walls = null;
	
	// Array of visited nodes
	boolean[][] visited = null;
	
	// Starting position to build maze
	int[] start = null;
	
	// Masks to remove from and to walls
	byte[] fromMask = { 14, 13, 11, 7 };
	byte[] toMask = { 13, 14, 7, 11 };
	
	// Array of position additions for direction
	int[][] adds = { { -1, 0 }, { 1, 0 }, { 0, 1 }, { 0, -1 } };
	
	// Map integers to hex digits
	String hexMap = "0123456789ABCDEF";
	
	// Method to initialize building process (can rebuild if needed)
	public void init() {
		// Create walls and visited arrays
		walls = new byte[height][width];
		visited = new boolean[height][width];
		for (int r = 0; r < height; r++)
			for (int c = 0; c < width; c++) {
				walls[r][c] = 15;
				visited[r][c] = false;
			}
	}
	
	ArrayList<Integer> getPermutation() {
		// Randomize directions
		ArrayList<Integer> dirs = new ArrayList<Integer>();
		dirs.add(0);
		dirs.add(1);
		dirs.add(2);
		dirs.add(3);
		java.util.Collections.shuffle(dirs);
		return dirs;
	}

	// Method to determine if new position is legal
	boolean isLegal(int[] p) {
		return (p[0] >= 0 && p[0] < height && p[1] >= 0 && p[1] < width && !visited[p[0]][p[1]]);
	}
	
	// Override toString from parent to return string of maze
	public String toString() {
		String ret = "";
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++)
				ret += hexMap.charAt(walls[r][c]);
			if (r < height - 1)
				ret += "\n";
		}
		return ret;
	}
	
	// Get the walls of the maze
	public byte[][] getWalls() {
		return walls;
	}

	// Method to set the starting position and build the maze
	public abstract void buildMaze();
	
}
