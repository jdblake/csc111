package queue;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueStaticTest {

	@Test
	public void testEnqueue() throws QueueException {
		QueueStatic<Integer> q = new QueueStatic<Integer>(100);
		for (int i = 0; i < 10; i++)
			q.enqueue(i);
		for (int i = 0; i < 5; i++)
			q.dequeue();
		assertEquals(q.peek(),new Integer(5));
	}

	@Test
	public void testTooBig() {
		QueueStatic<Integer> q = new QueueStatic<Integer>(10);
		try {
		for (int i = 0; i < 11; i++)
			q.enqueue(i);
		} catch (QueueException e) {
			assertTrue(true);
		}
		fail("Nope!");
	}

}
