package queue;

public class QueueStatic<T> implements QueueInterface<T> {
	
	private T[] data = null;
	private int start = 0;
	private int numItems = 0;
	private int LENGTH = 0;

	public QueueStatic(int l) {
		LENGTH = l;
		data = (T[]) new Object[LENGTH];
	}
	
	@Override
	public void enqueue(T item) throws QueueException {
		if (numItems == LENGTH)
			throw new QueueException("Trying to enqueue to full queue");
		data[(start+numItems)%LENGTH] = item;
		numItems++;
	}

	@Override
	public T dequeue() throws QueueException {
		if (isEmpty())
			throw new QueueException("Trying to dequeue from empty queue");
		T ret = data[start];
		numItems--;
		start = (start + 1) % LENGTH;
		return ret;
	}

	@Override
	public T peek() throws QueueException {
		if (isEmpty())
			throw new QueueException("Trying to peek from empty queue");
		return data[start];
	}

	@Override
	public boolean isEmpty() {
		return (numItems == 0);
	}

	@Override
	public void dequeueAll() {
		numItems = 0;
	}	

}
