package queue;

public interface QueueInterface<T> {

	public void enqueue(T item) throws QueueException ;
	public T dequeue() throws QueueException ;
	public T peek() throws QueueException ;
	public boolean isEmpty() ;
	public void dequeueAll() ;

}
