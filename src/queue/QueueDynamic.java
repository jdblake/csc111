package queue;

import stack.Node;

public class QueueDynamic<T> implements QueueInterface<T> {

	Node<T> head = null;
	Node<T> tail = null;

	public QueueDynamic() {
		super();
	}

	@Override
	public void enqueue(T item) throws QueueException {
		if (isEmpty())
			head = tail = new Node<T>(item, null);
		else {
			tail.setNext(new Node<T>(item, null));
			tail = tail.getNext();
		}
	}

	@Override
	public T dequeue() throws QueueException {
		if (isEmpty())
			throw new QueueException("Deleting empty queue");
		T item = head.getItem();
		head = head.getNext();
		if (head == null)
			tail = null;
		return item;
	}

	@Override
	public T peek() throws QueueException {
		if (isEmpty())
			throw new QueueException("Peeking empty queue");
		return (head.getItem());
	}

	@Override
	public boolean isEmpty() {
		return (head == null);
	}

	@Override
	public void dequeueAll() {
		head = tail = null;
	}

}
