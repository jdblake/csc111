package queue;

public class QueueException extends Exception {

	public QueueException(String str) {
		super(str);
	}
	
}
