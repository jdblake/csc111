package bank_account;

public class Account {

	public static final int ACCT_SAVINGS = 1;
	public static final int ACCT_CHECKING = 2;
	public static final int ACCT_HELOC = 3;
	public static final int ACCT_CD = 4;
	public static final int ACCT_MONEYMARKET = 5;

	private int acctBalance;
	private int acctNumber;
	private int acctType;
	private String acctName;

	public Account(int acctBalance, int acctNumber, int acctType, String acctName) {
		super();
		this.acctBalance = acctBalance;
		this.acctNumber = acctNumber;
		this.acctType = acctType;
		this.acctName = acctName;
	}

	public int getAcctNumber() {
		return acctNumber;
	}

	public boolean withdraw(int amt) {

		if ((amt > acctBalance) || (amt < 0)) return false;
		else {
			acctBalance -= amt;
			return true;
		}
	}

	public boolean deposit(int amt) {

		if (amt < 0) return false;
		else {
			acctBalance += amt;
			return true;
		}
	}

	@Override
	public String toString() {
		return "Account [acctBalance=" + acctBalance + ", acctNumber=" + acctNumber + ", acctType=" + acctType
				+ ", acctName=" + acctName + "]";
	}

}
