package bank_account;

import java.util.Scanner;

public class ATM {

	private static Scanner in = null;
	private static Account[] accts = new Account[10];
	private static int numAccounts = 0;

	private static void process(String cmd) {
		switch (cmd.toUpperCase().charAt(0)) {
		case 'Q':
			System.exit(1);
		case 'H':
			printHelp();
			break;
		case 'W':
			System.out.print("Enter account number: ");
			int numW = Integer.parseInt(in.nextLine());
			System.out.print("Enter amount of withdrawal: ");
			int amtW = Integer.parseInt(in.nextLine());
			Account curW = findAcct(numW);
			if (curW == null)
				System.out.println("Account " + numW + " not found!");
			else {
				if (curW.withdraw(amtW))
					System.out.println("Success!");
				else
					System.out.println("Insufficient Funds!");
			}
			break;
		case 'D':
			System.out.print("Enter account number: ");
			int numD = Integer.parseInt(in.nextLine());
			System.out.print("Enter amount of withdrawal: ");
			int amtD = Integer.parseInt(in.nextLine());
			Account curD = findAcct(numD);
			if (curD == null)
				System.out.println("Account " + numD + " not found!");
			else {
				if (curD.deposit(amtD))
					System.out.println("Success!");
				else
					System.out.println("Insufficient Funds!");
			}
			break;

		case 'C':
			if (numAccounts == accts.length)
				System.out.println("Too many accounts!");
			else {
				System.out.print("Enter account name: ");
				String nameC = in.nextLine();
				System.out.print("Enter account number: ");
				int numC = Integer.parseInt(in.nextLine());
				System.out.print("Enter account type: ");
				if (findAcct(numC) != null) {
					System.out.println("Account exists already!");
				} else {
					int typeC = Integer.parseInt(in.nextLine());
					accts[numAccounts++] = new Account(0, numC, typeC, nameC);
				}
			}
			break;
		case 'I':
			System.out.print("Enter account number: ");
			int numI = Integer.parseInt(in.nextLine());
			Account curI = findAcct(numI);
			if (curI == null)
				System.out.println("Account " + numI + " not found!");
			else
				System.out.println(curI);
			break;
		default:
			System.out.println("Command: " + cmd + " not known/");
			break;
		}
	}

	private static Account findAcct(int cur) {
		Account ret = null;
		for (int i = 0; (ret == null) && (i < numAccounts); i++) {
			if (accts[i].getAcctNumber() == cur)
				ret = accts[i];
		}
		return (ret);
	}

	private static void printHelp() {
		System.out.println("H -> Get help");
		System.out.println("W -> Withdraw money");
		System.out.println("D -> Deposit money");
		System.out.println("C -> Create account");
		System.out.println("I -> Get information");
	}

	public static void main(String[] args) {

		in = new Scanner(System.in);
		do {
			System.out.print("COMMAND> ");
			String response = in.nextLine();
			process(response);
		} while (true);

	}

}
