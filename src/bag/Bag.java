package bag;

public class Bag<T> implements Comparable<T>, Baggable<T> {

	private T[] items = null;
	private int size;
	private int numItems;

	// Constructor
	public Bag(int size) {
		this.size = size;
		empty();
		items = (T[]) new Object[size];
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#put(T)
	 */
	@Override
	public boolean put(T i) {
		if (isFull())
			return false;
		else {
			items[numItems++] = i;
			return true;
		}
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#removeLast()
	 */
	@Override
	public T removeLast() {
		T ret = null;
		if (!isEmpty()) {
			ret = items[--numItems];
		}
		return ret;
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#removeRand()
	 */
	@Override
	public T removeRand() {
		T ret = null;
		if (!isEmpty()) {
			int pos = new java.util.Random().nextInt(numItems);
			numItems--;
			ret = items[pos];
			for (int i = pos; i < numItems; i++) {
				items[i] = items[i+1];
			}
		}
		return ret;
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#count()
	 */
	@Override
	public int count() {
		return numItems;
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return (numItems == 0);
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#isFull()
	 */
	@Override
	public boolean isFull() {
		return (numItems == size);
	}

	// Finished
	/* (non-Javadoc)
	 * @see bag.Baggable#empty()
	 */
	@Override
	public void empty() {
		numItems = 0;
	}

	public int compareTo(T thing) {
		return numItems - ((Baggable) thing).count();
	}
	
}
