# ADT Bag #

### ADT Bag is a group of items like a bag of groceries. ###

* Items in the bag are in no particular order
* Items in the bag can be duplicated

Specifications:

* Put item in a bag
* Remove last item put in bag
* Remove random item put in bag
* Count items in bag
* Check it bag is empty
* Check if bag is full
* Empty bag

- public boolean put(Item i);
- public Item removeLast;
- public Item removeRand;
- public int count;
- public boolean isEmpty;
- public boolean isFull;
- public void empty;