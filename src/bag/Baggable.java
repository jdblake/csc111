package bag;

public interface Baggable<T> {

	// Finished
	boolean put(T i);

	// Finished
	T removeLast();

	// Finished
	T removeRand();

	// Finished
	int count();

	// Finished
	boolean isEmpty();

	// Finished
	boolean isFull();

	// Finished
	void empty();

}