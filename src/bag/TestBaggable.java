package bag;

public class TestBaggable {

	public static void main(String[] args) {

		Baggable<Integer> b1 = new AwesomeBag<Integer>(10);
		System.out.println(b1.count());
	    java.util.Random rand = new java.util.Random();
	    for (int i = 0; i< 10; i++)
	    	b1.put(rand.nextInt(100));
		System.out.println(b1.count());
		int t1 = b1.removeRand();
		int t2 = b1.removeRand();
	    System.out.println("RAND: " + b1.removeRand());
		System.out.println(b1.count());
		System.out.println(t1 + t2);
		int t3 = b1.removeLast();
		System.out.println("Last: " + t3);
	}


}
