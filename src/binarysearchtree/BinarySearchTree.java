package binarysearchtree;

// Class to represent a Binary Search Tree
public class BinarySearchTree<T extends Comparable> {

	// Root node
	private Node<T> root;
	
	// Variable indicating print order (pre/in/post)
	// 0 = preorder
	// 1 = inorder
	// 2 = postorder
	private int printOrder = 1;

	// Simple constructor
	public BinarySearchTree() {
		super();
		clear();
	}
	
	// Method to get the print order
	public int getPrintOrder() {
		return printOrder;
	}

	// Method to set the print order
	public void setPrintOrder(int printOrder) {
		this.printOrder = printOrder;
	}

	// Indicates that the tree is empty
	public boolean isEmpty() {
		return root == null;
	}

	// Clear the tree
	public void clear() {
		root = null;
	}

	// Method to find a node and parent (if there)
	public Node<T>[] find(T item) {
		// Make return array
		Node<T>[] ret = new Node[2];
		// Start searching at the root
		Node<T> p = root;
		Node<T> c = null;
		// As long as the node hasn't been found, and we haven't fallen
		// off the tree, keep looking
		while (p != null && p.getData().compareTo(item) != 0) {
			// Keep track of the parent
			c = p;
			// Move to the appropriate child
			if (p.getData().compareTo(item) < 0)
				p = p.getRight();
			else
				p = p.getLeft();
		}
		// Return the node and parent
		ret[0] = p;
		ret[1] = c;
		return ret;
	}

	// Method to add a node to the tree
	public boolean add(T item) {
		// Assume the insert will work
		boolean ret = true;
		// Special case for empty tree
		if (isEmpty()) {
			root = new Node<T>(item, null, null);
		} else {
			// Find the node (and parent)
			Node<T>[] f = find(item);
			// If the node is there, do not add
			if (f[0] != null)
				ret = false;
			else {
				// Make new node and add to left or right as necessary
				Node<T> n = new Node<T>(item, null, null);
				if (f[1].getData().compareTo(item) < 0)
					f[1].setRight(n);
				else
					f[1].setLeft(n);
			}
		}
		return ret;

	}

	// Recursive method to return an inOrder String representation
	private String inOrder(Node<T> cur) {
		if (cur == null)
			return "";
		else
			return "(" + inOrder(cur.getLeft()) + cur.toString() + inOrder(cur.getRight()) + ")";
	}

	// Recursive method to return an postOrder String representation
	private String postOrder(Node<T> cur) {
		if (cur == null)
			return "";
		else
			return "(" + postOrder(cur.getLeft()) + postOrder(cur.getRight()) + cur.toString() + ")";
	}

	// Recursive method to return an preOrder String representation
	private String preOrder(Node<T> cur) {
		if (cur == null)
			return "";
		else
			return "(" + cur.toString() + preOrder(cur.getLeft()) + preOrder(cur.getRight()) + ")";
	}

	// toString calling the appropriate string builder
	public String toString() {
		String ret = "";
		switch (printOrder) {
		case 0:
			ret = preOrder(root);
			break;
		case 1:
			ret = inOrder(root);
			break;
		case 3:
			ret = postOrder(root);
			break;
		}
		return ret;
	}

	// Method to remove a node from the tree
	public boolean remove(T item) {
		// Set up initial return value (assume it will fail)
		boolean ret = false;
		// Find the node to remove
		Node<T>[] p = find(item);
		// Run if the node was there
		if (p[0] != null) {
			// Node has 1 or 0 children
			if (p[0].getLeft() == null || p[0].getRight() == null) {
				// Find node to replce current one
				Node<T> tmp = null;
				if (p[0].getLeft() == null)
					tmp = p[0].getRight();
				else
					tmp = p[0].getLeft();
				// If deleting root, make it equal to replacement node
				if (p[0] == root)
					root = tmp;
				// Move replacement to child position of old parent
				else {
					if (p[1].getData().compareTo(item) < 0)
						p[1].setRight(tmp);
					else
						p[1].setLeft(tmp);
				}
			} else {
				// Node has 2 children
				// Find next next largest node
				Node<T> cur = p[0].getRight();
				Node<T> prev = null;
				while (cur.getLeft() != null) {
					prev = cur;
					cur = cur.getLeft();
				}
				// Move next largest data up
				p[0].setData(cur.getData());
				// Next largest is right child
				if (prev == null) {
					p[0].setRight(cur.getRight());
				} else
					prev.setLeft(cur.getRight());
			}
			// Node deleted!
			ret = true;
		}
		return ret;
	}

}
