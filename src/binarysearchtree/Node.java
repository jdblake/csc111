package binarysearchtree;

// Class representing a Node in a Binary Search Tree
public class Node<T> {

	// Data
	private T data;
	// Left and right pointers
	private Node<T> left;
	private Node<T> right;
	
	// Public constructor
	public Node(T d, Node<T> l, Node<T> r) {
		super();
		data = d;
		left = l;
		right = r;
	}

	// Get the data
	public T getData() {
		return data;
	}

	// Set the data
	public void setData(T data) {
		this.data = data;
	}

	// Get the left node
	public Node<T> getLeft() {
		return left;
	}

	// Set the left node
	public void setLeft(Node<T> left) {
		this.left = left;
	}

	// Get the right node
	public Node<T> getRight() {
		return right;
	}

	// Set the right node
	public void setRight(Node<T> right) {
		this.right = right;
	}
	
	// Simple toString to return the data from the node
	public String toString() { return data.toString(); }
	
}
