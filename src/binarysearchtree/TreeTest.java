package binarysearchtree;

import static org.junit.Assert.*;

import org.junit.Test;

public class TreeTest {

	@Test
	public void testCreateNodeInteger() {
		Node<Integer> n = new Node<Integer>(3,null,null);
		assertNotNull(n);
	}

	@Test
	public void testCreateNodeString() {
		Node<String> n = new Node<String>("hello",null,null);
		assertNotNull(n);
	}

	@Test
	public void testCreateTreeString() {
		BinarySearchTree<String> t = new BinarySearchTree<String>();
		assertNotNull(t);
	}

	@Test
	public void testCreateTreeInteger() {
		BinarySearchTree<Integer> t = new BinarySearchTree<Integer>();
		assertNotNull(t);
	}

	@Test
	public void testNewTreeEmpty() {
		BinarySearchTree<Integer> t = new BinarySearchTree<Integer>();
		assertTrue(t.isEmpty());
	}

	@Test
	public void testTreeNotEmpty() {
		BinarySearchTree<Integer> t = new BinarySearchTree<Integer>();
		t.add(3);
		assertTrue(!t.isEmpty());
	}

	@Test
	public void testFind() {
		BinarySearchTree<Integer> t = new BinarySearchTree<Integer>();
		for (int i = 0; i < 100; i++) {
			t.add((int) (Math.random() * 1000));
		}
		t.add(3);
		Node<Integer>[] r = t.find(3);
		assertTrue((r[0] != null));
	}

	@Test
	public void testToString1() {
		BinarySearchTree<Integer> t = new BinarySearchTree<Integer>();
		assertEquals(t.toString(),"");
	}

	@Test
	public void testToStringInOrder() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.add(20);
		tree.add(10);
		tree.add(25);
		tree.add(75);
		tree.add(90);
		tree.add(90);
		tree.add(90);
		tree.add(55);
		tree.add(65);
		assertEquals(tree.toString(),"(((10)20(25))30((55(65))75(90)))");
	}
	
	@Test
	public void testRemove1() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.remove(30);
		assertTrue(tree.isEmpty());
	}

	@Test
	public void testToRemove2() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.add(20);
		tree.add(10);
		tree.add(25);
		tree.add(75);
		tree.add(90);
		tree.add(90);
		tree.add(90);
		tree.add(55);
		tree.add(65);
		tree.remove(20);
		assertEquals(tree.toString(),"(((10)25)30((55(65))75(90)))");
	}

	@Test
	public void testToRemove3() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.add(20);
		tree.add(10);
		tree.add(25);
		tree.add(75);
		tree.add(90);
		tree.add(90);
		tree.add(90);
		tree.add(55);
		tree.add(65);
		tree.remove(30);
		assertEquals(tree.toString(),"(((10)20(25))55((65)75(90)))");
	}
	
	@Test
	public void testRemove4() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.add(50);
		tree.remove(30);
		assertEquals(tree.toString(),"(50)");
	}
	
	@Test
	public void testRemove5() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(30);
		tree.add(20);
		tree.remove(20);
		assertEquals(tree.toString(),"(30)");
	}



}
