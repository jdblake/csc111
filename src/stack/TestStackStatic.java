package stack;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestStackStatic {

	@Test
	public void testStack() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		assertNotNull(s);
	}

	@Test
	public void testPop() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		try {
			s.push(15);
			s.push(12);
			assertEquals((int) s.pop(),12);
		} catch (StackSizeException e) { }
	}

	@Test
	public void testPush() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		try {
			s.push(15);
			s.push(12);
			assertFalse(s.isEmpty());
		} catch (StackSizeException e) { }
	}

	@Test
	public void testPeek() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		try {
			s.push(15);
			s.push(12);
			assertEquals((int) s.peek(),12);
		} catch (StackSizeException e) { }
	}

	@Test
	public void testIsEmpty1() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		assertTrue(s.isEmpty());
	}

	@Test
	public void testIsEmpty2() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		try {
			s.push(15);
			s.push(12);
			assertEquals((int) s.peek(),12);
		} catch (StackSizeException e) { }
		assertFalse(s.isEmpty());
	}

	@Test
	public void testClear() {
		StackInterface<Integer> s = new StackStatic<Integer>(10);
		try {
			s.push(15);
			s.push(12);
			s.clear();
			assertTrue(s.isEmpty());
		} catch (StackSizeException e) { }

	}

}
