package stack;

public class StackStatic<T> implements StackInterface<T> {

	private T[] data = null;
	private int maxSize = 0;
	private int numItems = 0;
	
	public StackStatic(int size) {
		data = (T[]) new Object[size];
		maxSize = size;
	}

	@Override
	public T pop() throws StackSizeException {
		if (isEmpty())
			throw new StackSizeException("Pop from empty Stack");
		return data[--numItems];
	}

	@Override
	public void push(T item) throws StackSizeException {
		if (numItems == maxSize)
			throw new StackSizeException("Push onto full Stack");
		data[numItems++] = item;
	}

	@Override
	public T peek() throws StackSizeException {
		if (isEmpty())
			throw new StackSizeException("Peek on empty Stack");
		return data[numItems-1];
	}

	@Override
	public boolean isEmpty() {
		return numItems == 0;
	}

	@Override
	public void clear() {
		numItems = 0;
	}
	
}
