package stack;

public class StackSizeException extends Exception {

	public StackSizeException(String message) {
		super(message);
	}

}
