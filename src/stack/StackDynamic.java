package stack;

public class StackDynamic<T> implements StackInterface<T> {
	
	private StackLinkedList<T> list = null;
	
	public StackDynamic() {
		list = new StackLinkedList<T>();
	}

	@Override
	public T pop() throws StackSizeException {
		return list.remove();
	}

	@Override
	public void push(T item) throws StackSizeException {
		list.add(item);
	}

	@Override
	public T peek() throws StackSizeException {
		T tmp = list.remove();
		list.add(tmp);
		return tmp;
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public void clear() {
		list.clear();
	}

}
