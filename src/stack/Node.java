package stack;

public class Node<T> {
	
	private T item = null;
	private Node<T> next = null;
	
	public Node(T item, Node<T> n) {
		this.item = item;
		next = n;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public Node<T> getNext() {
		return next;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

}
