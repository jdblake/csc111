package stack;

public interface StackInterface<T> {
	
	public T pop() throws StackSizeException ;
	public void push(T item) throws StackSizeException ;
	public T peek() throws StackSizeException ;
	public boolean isEmpty();
	public void clear();

}
