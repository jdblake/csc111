package stack;

public class StackLinkedList<T> {
	
	private Node<T> head = null;
	
	public StackLinkedList() {
		head = null;
	}
	
	public boolean isEmpty() {
		return head == null;
	}
	
	public void add(T item) {
		head = new Node<T>(item, head);
	}
	
	public T remove() {
		T item = head.getItem();
		head = head.getNext();
		return item;
	}
	
	public void clear() {
		head = null;
	}
	
}
