# 16/SP CSC111 Repository #

### Steps to clone/open this repository in Eclipse ###

* Clone the repository on your disk with:
```text
git clone https://jdblake@bitbucket.org/jdblake/csc111.git
```
* Open Eclipse and create a new Java project (name it whatever you like)
* De-select "Choose Default Location" and browse to where you saved the clone (the csc111 directory)
* Click "Finish"